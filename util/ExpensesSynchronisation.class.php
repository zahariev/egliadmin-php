<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class ExpensesSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS expenses;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS expenses (id varchar(255) PRIMARY KEY, txt varchar(255));");
	}
	
	public function fillTable(){
		$sql_read = "select * from vieAPPSpesenarten";
		$sql_write = "INSERT INTO expenses ( id, txt) VALUES ( ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				$keys = array();
				foreach ($row as $key => $element){
					if (is_string($element))
						$row[$key] = trim($element);
					$keys[] = $key;
				}
						
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row[$keys[0]]);
				$stmt->bindParam(2, $row["SpesenArt"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
	}
}

?>