<?php 

require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class ArticleSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS articleGroups;");
		$this->target_link->query("DROP TABLE IF EXISTS articles;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS articles ( ".
					"id integer PRIMARY KEY AUTOINCREMENT, ".
					"category INT NOT NULL, ".
					"artNr INT, ".
					"pktStk INT, ".
					"titleDe VARCHAR(255), ".
					"titleFr VARCHAR(255), ".
					"titleIt VARCHAR(255), ".
					"palette INT, ".
					"modiNr INT, ".
					"inhalt INT, ".
					"price FLOAT, ".
					"kp FLOAT, ".
					"liefNr INT, ".
					"image BOOLEAN, ".
					"imageDate DATETIME, ".
					"mutationDate DATE, ".
					"ean1 VARCHAR(255), ".
					"ean2 VARCHAR(255), ".
					"ean3 VARCHAR(255), ".
					"eigenMarke BOOLEAN, ".
					"stock BOOLEAN, ".
					"unitDe VARCHAR(255), ". 
					"unitFr VARCHAR(255) ".
				");");
		$this->target_link->query("CREATE TABLE IF NOT EXISTS articleGroups ( id integer PRIMARY KEY AUTOINCREMENT, parentId VARCHAR(255), titleDe VARCHAR(255), titleFr VARCHAR(255), titleIt VARCHAR(255), sort INT );");
	}
	
	public function fillTable(){
		$sql_read = "SELECT a.Titelstufe1, a.Titel1D, a.Titel1F, a.Titel1I FROM vieAPPArtikelgruppen a GROUP BY Titelstufe1, Titel1D, Titel1F, Titel1I";
		$sql_write = "INSERT INTO articleGroups ( parentId, titleDe, titleFr, titleIt, sort) VALUES ( 0, ?, ?, ?, ? );";
		$sort = 0;
		try {
			$stmt = $this->target_link->prepare($sql_write);
			$rows = $this->source_link->query($sql_read);
			foreach ($rows as $row) {
				$sort++;
				$stmt->bindParam(1, $row["Titel1D"]);
				$stmt->bindParam(2, $row["Titel1F"]);
				$stmt->bindParam(3, $row["Titel1I"]);
				$stmt->bindParam(4, $sort);
				$stmt->execute();
				$lastID = $this->target_link->lastInsertId();
				
				$sort = $this->handleLevel2($lastID, $row["Titelstufe1"], $sort);
				$this->createProductsFromGroup($lastID, $row["Titelstufe1"], null, null);
			}
			
		}
		catch (Exception $e){
			echo "Exception while synchronising: ".__LINE__.$e->getMessage()."<br>\n";
		}
	}
	
	public function handleLevel2($parent, $stufe1, $currentSort){
		$sort = $currentSort;
		$sql_read = "SELECT a.Titelstufe2, a.Titel2D, a.Titel2F, a.Titel2I FROM vieAPPArtikelgruppen a WHERE Titelstufe1=".$stufe1." and Titel2D != '' GROUP BY Titelstufe2, Titel2D, Titel2F, Titel2I";
		$sql_write = "INSERT INTO articleGroups ( parentId, titleDe, titleFr, titleIt, sort) VALUES ( ?, ?, ?, ?, ? );";
		try {
			$stmt = $this->target_link->prepare($sql_write);
			$rows = $this->source_link->query($sql_read);
			foreach ($rows as $row) {
				$sort++;
				$stmt->bindParam(1, intVal($parent));
				$stmt->bindParam(2, $row["Titel2D"]);
				$stmt->bindParam(3, $row["Titel2F"]);
				$stmt->bindParam(4, $row["Titel2I"]);
				$stmt->bindParam(5, $sort);
				$stmt->execute();
				$lastID = $this->target_link->lastInsertId();
				
				$sort = $this->handleLevel3($lastID, $stufe1, $row["Titelstufe2"], $sort);
				$this->createProductsFromGroup($lastID, $stufe1, $row["Titelstufe2"], null);
			}
			
		}
		catch (Exception $e){
			echo "Exception while synchronising: ".__LINE__.$e->getMessage()."<br>\n";
		}
		return $sort;
	}
	
	public function handleLevel3($parent, $stufe1, $stufe2, $currentSort){
		$sort = $currentSort;
		$sql_read = "SELECT a.Titelstufe3, a.Titel3D, a.Titel3F, a.Titel3I FROM vieAPPArtikelgruppen a WHERE Titelstufe1=".$stufe1." and Titelstufe2=".$stufe2." and Titel3D != '' GROUP BY Titelstufe3, Titel3D, Titel3F, Titel3I";
		$sql_write = "INSERT INTO articleGroups ( parentId, titleDe, titleFr, titleIt, sort) VALUES ( ?, ?, ?, ?, ? );";
		try {
			$stmt = $this->target_link->prepare($sql_write);
			$rows = $this->source_link->query($sql_read);
			foreach ($rows as $row) {
				$sort++;
				$stmt->bindParam(1, intVal($parent));
				$stmt->bindParam(2, $row["Titel3D"]);
				$stmt->bindParam(3, $row["Titel3F"]);
				$stmt->bindParam(4, $row["Titel3I"]);
				$stmt->bindParam(5, $sort);
				$stmt->execute();
				
				$lastID = $this->target_link->lastInsertId();
				$this->createProductsFromGroup($lastID, $stufe1, $stufe2, $row["Titelstufe3"]);
			}
			
		}
		catch (Exception $e){
			echo "Exception while synchronising: ".__LINE__.$e->getMessage()."<br>\n";
		}
		return $sort;
	}
	
	public function createProductsFromGroup($sqliteId, $groupLevel1, $groupLevel2, $groupLevel3){
		$sql_read = "SELECT * FROM vieAPPArtikel WHERE Titelstufe1 = ".$groupLevel1;
		if($groupLevel2 != null) {
			$sql_read.= " AND Titelstufe2 = ".$groupLevel2;
		} else {
			$sql_read.= " AND Titelstufe2 = 1";
		}
		if($groupLevel3 != null) {
			$sql_read.= " AND Titelstufe3 = ".$groupLevel3;
		} else {
			$sql_read.= " AND Titelstufe3 = 1";
		}
		
		$sql_write = "INSERT INTO articles ".
				"(category, artNr, pktStk, titleDe, titleFr, titleIt, palette, modiNr, inhalt, price, kp, liefNr, image, imageDate, mutationDate, ean1, ean2, ean2, eigenMarke, stock, unitDe, unitFr, id) ".
				"VALUES ".
				"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		$stmt = $this->target_link->prepare($sql_write);
		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				$stmt->bindParam(1, $sqliteId);
				$stmt->bindParam(2, intval($row["art_nr"]));
				$stmt->bindParam(3, intval($row["pkt_st"]));
				$stmt->bindParam(4, $row["de"]);
				$stmt->bindParam(5, $row["fr"]);
				$stmt->bindParam(6, $row["it"]);
				$stmt->bindParam(7, intval($row["palette"]));
				$stmt->bindParam(8, intval($row["modi_nr"]));
				$stmt->bindParam(9, intval($row["Inhalt"]));
				$stmt->bindParam(10, floatval(str_replace(",", ".", $row["prs"])));
				$stmt->bindParam(11, floatval(str_replace(",", ".", $row["KP"])));
				$stmt->bindParam(12, $row["LiefNr"]);
				$foundImage = $this->handleImage($row["art_nr"]);
				$stmt->bindParam(13, $row["ArtikelImage"]);
				$stmt->bindParam(14, $row["ImgDat"]);
				$stmt->bindParam(15, $row["MutDat"]);
				$stmt->bindParam(16, $row["AREAN1"]);
				$stmt->bindParam(17, $row["AREAN2"]);
				$stmt->bindParam(18, $row["AREAN3"]);
				$stmt->bindParam(19, $row["EigenMarke"]);
				$stmt->bindParam(20, $row["Lieferstatus"]);
				$stmt->bindParam(21, $row["EinheitD"]);
				$stmt->bindParam(22, $row["EinheitF"]);
				$stmt->bindParam(23, intval($row["art_nr"]));
				$stmt->execute();
				echo "Writing productid ".$row["art_nr"]." from category ".$sqliteId."\n";
			}
			catch (Exception $e){
				echo "Exception while synchronising articleID:".$row["art_nr"].", category: ".$sqliteId.": ".__LINE__.$e->getMessage()."<br>\n";
			}
		}
		
	}
	
	public function handleImage($id){
		require(dirname(__DIR__).DIRECTORY_SEPARATOR."config.php");
		$source_file = $egli_dir_image.DIRECTORY_SEPARATOR.$id.".jpg";
		$target_file = $egli_dir_tmp.DIRECTORY_SEPARATOR.$id.".jpg";
		
		if (file_exists($source_file)){
			if (!copy($source_file, $target_file))
				return false;
			$source_file = $egli_dir_thumbs.DIRECTORY_SEPARATOR.$id.".jpg";
			$target_file = $egli_dir_tmp.DIRECTORY_SEPARATOR.$id."_grid.jpg";
			if (file_exists($source_file)){
				if (!copy($source_file, $target_file))
					return false;
				else
					return true;
			}
			else
				return false;
		}
		else
			return false;
	}
}

?>