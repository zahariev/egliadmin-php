<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class CustomerPricesSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS customerprices;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS customerprices (customer integer, article integer, price float, vprice float, PRIMARY KEY (customer, article));");
	}
	
	public function fillTable(){
		/*$i = 0;
		$sql_read = "select n.* from vieAPPNettoPreise n left join vieAPPArtikel a on a.art_nr=n.ArtNr where n.AP != a.prs or n.VP != a.KP";
		$sql_write = "INSERT INTO customerprices ( customer, article, price, vprice ) VALUES ( ?, ?, ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				$i++;
				if ($i%100 == 0)
					echo $i." customer prices processed \n";
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["KdNr"]);
				$stmt->bindParam(2, $row["ArtNr"]);
				$stmt->bindParam(3, $row["AP"]);
				$stmt->bindParam(4, $row["VP"]);
				$stmt->execute();
			}
			catch (Exception $e){
				//echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}*/
	}
}

?>