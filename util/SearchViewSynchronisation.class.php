<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class SearchViewSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){}
	
	public function createTable(){
		$this->target_link->query("CREATE VIEW IF NOT EXISTS ".
				"search ".
			"AS ".
				"SELECT ".
					"CASE WHEN ag3.ROWID IS NULL ".
					"THEN ".
						"ag2.ROWID ".
					"ELSE ".
						"ag3.ROWID ".
					"END AS categoryId, ".
					"CASE WHEN ag3.ROWID IS NULL ".
					"THEN ".
						"ag1.titleDe || ' > ' || ag2.titleDe ".
					"ELSE ".
						"ag1.titleDe || ' > ' || ag2.titleDe || ' > ' || ag3.titleDe ".
					"END AS pathDe, ".
					"CASE WHEN ag3.ROWID IS NULL ".
					"THEN ".
						"ag1.titleFr || ' > ' || ag2.titleFr ".
					"ELSE ".
						"ag1.titleFr || ' > ' || ag2.titleFr || ' > ' || ag3.titleFr ".
					"END AS pathFr, ".
					"CASE WHEN ag3.ROWID IS NULL ".
					"THEN ".
						"COUNT(ag1.ROWID || '-' || ag2.ROWID) ".
					"ELSE ".
						"COUNT(ag1.ROWID || '-' || ag2.ROWID || '-' || ag3.ROWID) ".
					"END AS articleCount ".
				"FROM ".
			  		"articleGroups AS ag1 ".
				"LEFT JOIN ".
			  		"articleGroups AS ag2 ".
				"ON ".
					"ag1.ROWID = ag2.parentId ".
				"LEFT JOIN ".
					"articleGroups AS ag3 ".
				"ON ".
					"ag2.ROWID = ag3.parentId ".
				"LEFT JOIN ".
					"articles AS art2 ".
				"ON ".
					"ag2.ROWID = art2.category ".
				"LEFT JOIN ".
					"articles AS art3 ".
				"ON ".
					"ag3.ROWID = art3.category ".
				"WHERE ".
					"ag1.parentId = 0 ".
				"GROUP BY ".
					"ag1.ROWID, ".
					"ag2.ROWID, ".
					"ag3.ROWID;");
	}
	
	public function fillTable(){}
}

?>