<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class VisitSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS visits;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS visits (id varchar(255) PRIMARY KEY, txt varchar(255));");
	}
	
	public function fillTable(){
		$sql_read = "select * from vieAPPBesuchsarten";
		$sql_write = "INSERT INTO visits ( id, txt) VALUES ( ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				foreach ($row as $key => $element)
					if (is_string($element))
						$row[$key] = trim($element);
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["AktivitaetenId"]);
				$stmt->bindParam(2, $row["AktivitaetenText"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
	}
}

?>