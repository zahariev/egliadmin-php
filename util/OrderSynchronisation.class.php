<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class OrderSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	private $currentId = 0;
	private $orderNumber = 0;
	private $total = 0;
	private $agents;
	private $old_row;
	
	function __construct($source, $target, $agents){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->agents = $agents;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS orders;");
		$this->target_link->query("DROP TABLE IF EXISTS orderItems;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS orders (agentId INTEGER NOT NULL, customerId INTEGER NOT NULL, total NUMERIC(25), email VARCHAR(255), delivered INTEGER(1), surcharge NUMERIC(25), message VARCHAR(255), deliveryDate VARCHAR(25), dateOrdered VARCHAR(25), billId integer NOT NULL, status INTEGER(1) NOT NULL DEFAULT 0);");
		$this->target_link->query("CREATE TABLE IF NOT EXISTS orderItems (orderId INTEGER NOT NULL, articleId INTEGER NOT NULL, artNr VARCHAR(25), image INTEGER(1) NOT NULL DEFAULT 0, title VARCHAR(255), amount INTEGER NOT NULL, price NUMERIC(25), unit VARCHAR(25), total NUMERIC(25), FOREIGN KEY(orderId) REFERENCES orders(ROWID));");
	}
	
	public function fillTable(){
		$sql_read = "select b.*, a.art_nr as productId, a.ArtikelImage, a.prs, a.EinheitD from tblAPPBestellungen b left join vieAPPArtikel a on b.Best_Art_Nr=a.art_nr";
		$sql_write = "INSERT INTO orderItems ( orderId, articleId, artNr, image, title, amount, price, unit, total) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				if ($this->orderNumber != $row["Best_Nr"]){
					if ($this->orderNumber != 0)
						$this->insertOrder($old_row);
					$this->currentId++;
					$this->orderNumber = $row["Best_Nr"];
					$this->total = 0;
				}
				if (empty($row["productId"]))
					$row["productId"] = 0;
				if (empty($row["ArtikelImage"]))
					$row["ArtikelImage"] = false;
				if (empty($row["EinheitD"]))
					$row["EinheitD"] = "";
				if (empty($row["prs"]))
					$row["prs"] = 0;
				
				$sum = $row["Best_Menge"]*$row["prs"];
				$this->total+= $sum;
				
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $this->currentId);
				$stmt->bindParam(2, $row["productId"]);
				$stmt->bindParam(3, $row["productId"]);
				$stmt->bindParam(4, $row["ArtikelImage"]);
				$stmt->bindParam(5, $row["Best_Art_Name"]);
				$stmt->bindParam(6, $row["Best_Menge"]);
				$stmt->bindParam(7, $row["prs"]);
				$stmt->bindParam(8, $row["EinheitD"]);
				$stmt->bindParam(9, $sum);
				$stmt->execute();
				
				$this->old_row = $row;
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
	}
	
	private function insertOrder($row){
		$sql_write = "insert into orders ".
			"(agentId, customerId, total, email, delivered, surcharge, message, ".
			"deliveryDate, dateOrdered, billId, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 2);";
		try{
		$stmt = $this->target_link->prepare($sql_write);
		$stmt = $this->target_link->prepare($sql_write);
		$agent = $this->agents[$row["Best_ADLogin"]];
		if (empty($agent))
			$agent = -1;
		if ($row["Best_Kleinmenge"] == 1)
			$this->total+= 20;
		$stmt->bindParam(1, $this->agents[$row["Best_ADLogin"]]);
		$stmt->bindParam(2, $row["Best_Kd_Nr"]);
		$stmt->bindParam(3, $this->total);
		$stmt->bindParam(4, $row["Best_Mail"]);
		$stmt->bindParam(5, $row["Best_Auto"]);
		$stmt->bindParam(6, $row["Best_Kleinmenge"]);
		$stmt->bindParam(7, $row["Best_Kommentar"]);
		$stmt->bindParam(8, date("Y-m-d", strtotime($row["Best_Lief_Datum"])));
		$stmt->bindParam(9, strtotime($row["Best_Time"]));
		$stmt->bindParam(10, $this->orderNumber);
		}
		catch (Exception $e){
			echo "Order synchronisation: ".$e->getMessage()."\n";
		}
	}
}

?>