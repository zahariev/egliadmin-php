<?php 

require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class MutationSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS articleMutations");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS articleMutations ( ".
                    "id INT PRIMARY KEY, ".
                    "artNr INT, ".
                    "date VARCHAR(25), ".
                    "description VARCHAR(250), ".
                    "field VARCHAR(250), ".
                    "oldValue VARCHAR(250), ".
                    "newValue VARCHAR(250) ".
            ");");
	}
	
	public function fillTable(){
		$sql_read = "select * from vieAPPArtikelMutationen";
		$sql_write = "INSERT INTO articleMutations ( `id`, `artNr`, `date`, `description`, `field`, `oldValue`, `newValue` ) VALUES ( ?, ?, ?, ?, ?, ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["LogId"]);
				$stmt->bindParam(2, intval($row["LogARANR"]));
				$stmt->bindParam(3, $row["LogDate"]);
				$stmt->bindParam(4, $row["LogDescription"]);
				$stmt->bindParam(5, $row["LogField"]);
				$stmt->bindParam(6, $row["LogOldValue"]);
				$stmt->bindParam(7, $row["LogNewValue"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
		
	}
}

?>