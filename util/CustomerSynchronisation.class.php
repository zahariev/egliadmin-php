<?php 

require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class CustomerSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS customers;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS customers (id integer PRIMARY KEY AUTOINCREMENT, customer_id integer, lastname VARCHAR(255), firstname VARCHAR(255), address VARCHAR(255), company VARCHAR(255), zipcode VARCHAR(20), city VARCHAR(255), language integer, phone VARCHAR(255), phone2 VARCHAR(255), fax VARCHAR(255), email VARCHAR(255), address2 VARCHAR(255), title VARCHAR(255), agent varchar(255), agent2 varchar(255), synched tinyint, appointment long, periodicity integer);");
	}
	
	public function fillTable(){
		$sql_read = "select ap.Termin, ks.*, kk.NName, kk.VName, kk.TelVorwahl, kk.TelNr, kk.NatelVorwahl, kk.NatelNr, kk.FaxVorwahl as cdFaxVorwahl, kk.FaxNr as cdFaxNr from vieAPPKundenstamm ks left join vieAppKundenKontakte kk on ks.kd_nr=kk.Kundennummer left join vieAppperiode ap on ks.kd_nr=ap.kd_nr";
		$sql_write = "INSERT INTO customers ( customer_id, lastname, firstname, address, company, zipcode, city, language, phone, phone2, email, address2, title, agent, agent2, synched, id, appointment, periodicity, fax ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, ?, ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				foreach ($row as $key => $element)
					if (is_string($element))
						$row[$key] = trim($element);
					
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["kd_nr"]);
				
				if (!empty($row["NName"]))
					$stmt->bindParam(2, $row["NName"]);
				else
					$stmt->bindParam(2, $row["KDNAM2"]);
				if (!empty($row["VName"]))
					$stmt->bindParam(3, $row["VName"]);
				else
					$stmt->bindParam(3, $row["v_name"]);
					
				$stmt->bindParam(4, $row["adresse"]);
				$stmt->bindParam(5, $row["n_name"]);
				$stmt->bindParam(6, $row["plz"]);
				$stmt->bindParam(7, $row["ort"]);
				$stmt->bindParam(8, $row["sprache"]);
				$phone = $this->getPhone($row);
				$stmt->bindParam(9, $phone);
				$phone2 = $this->getPhone2($row);
				$stmt->bindParam(10, $phone2);
				if (empty($row["Mail"]))
					$row["Mail"] = "";
				$stmt->bindParam(11, $row["Mail"]);
				$stmt->bindParam(12, $row["adresse2"]);
				$title = "";
				$stmt->bindParam(13, $title);
				$stmt->bindParam(14, $row["AD1"]);
				$stmt->bindParam(15, $row["AD2"]);
				$stmt->bindParam(16, $row["kd_nr"]);
				$row["Termin"] = strtotime($row["Termin"])*1000;
				$stmt->bindParam(17, $row["Termin"]);
				if (empty($row["Periodizitaet"]))
					$row["Periodizitaet"] = 0;
				$stmt->bindParam(18, $row["Periodizitaet"]);
				$fax = $this->getFax($row);
				$stmt->bindParam(19, $fax);
				
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising Customerid ".$row["kd_nr"].": ".$e->getMessage()."<br>\n";
			}
		}
		
	}
	
	public function getPhone($row){
		if (empty($row["Tel1Nr"])){
			if (!empty($row["TelVorwahl"]))
				return "(0".$row["TelVorwahl"].")".$row["TelNr"];
			else
				return "".$row["TelNr"];
		}
		else {
			if (!empty($row["Tel1Vorw"]))
				return "(0".$row["Tel1Vorw"].")".$row["Tel1Nr"];
			else
				return "".$row["Tel1Nr"];
		}
	}
	
	public function getPhone2($row){
		if (empty($row["Tel2Nr"])){
			if (!empty($row["NatelVorwahl"]))
				return "(0".$row["NatelVorwahl"].")".$row["NatelNr"];
			else
				return "".$row["TelNr"];
		}
		else {
			if (!empty($row["Tel2Vorw"]))
				return "(0".$row["Tel2Vorw"].")".$row["Tel2Nr"];
			else
				return "".$row["Tel2Nr"];
		}
	}
	
	public function getFax($row){
		if (empty($row["FaxNr"])){
			if (!empty($row["cdFaxVorwahl"]))
				return "(0".$row["cdFaxVorwahl"].")".$row["cdFaxNr"];
			else
				return "".$row["cdFaxNr"];
		}
		else {
			if (!empty($row["FaxVorw"]))
				return "(0".$row["FaxVorw"].")".$row["FaxNr"];
			else
				return "".$row["FaxNr"];
		}
	}
}

?>