<?php 

require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class CRMSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS crm;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS crm (id integer PRIMARY KEY AUTOINCREMENT, customer integer, type varchar(255), date long, agent varchar(255), notice text, expenses double);");
	}
	
	public function fillTable(){
		$sql_read = "select * from vieAPPcrm";
		$sql_write = "INSERT INTO crm ( id, customer, type, date, agent, notice, expenses) VALUES (NULL, ?, ?, ?, ?, ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["KDKDNR"]);
				$stmt->bindParam(2, intval($row["Typ"]));
				$timestamp = strtotime($row["Datum"])*1000;
				$stmt->bindParam(3, $timestamp);
				$stmt->bindParam(4, $row["UserName"]);
				$stmt->bindParam(5, $row["Notiz"]);
				$stmt->bindParam(6, $row["Spesen"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
		
	}
}

?>