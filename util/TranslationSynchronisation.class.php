<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class TranslationSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS translation;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS translation (id integer PRIMARY KEY AUTOINCREMENT, name varchar(255), txt_de TEXT, txt_fr TEXT, txt_it TEXT);");
	}
	
	public function fillTable(){
		$sql_read = "select * from tblAPPTranslations";
		$sql_write = "INSERT INTO translation ( name, txt_de, txt_fr, txt_it ) VALUES ( ?, ?, ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["name"]);
				$stmt->bindParam(2, $row["txt_de"]);
				$stmt->bindParam(3, $row["txt_fr"]);
				$stmt->bindParam(4, $row["txt_it"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
	}
}

?>