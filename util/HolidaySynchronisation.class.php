<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class HolidaySynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS Holiday;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS Holiday (name varchar(255), date datetime);");
	}
	
	public function fillTable(){
		$sql_read = "select * from tblAPPHoliday";
		$sql_write = "INSERT INTO Holiday ( name, date) VALUES ( ?, ? );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				foreach ($row as $key => $element)
					if (is_string($element))
						$row[$key] = trim($element);
						
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["Name"]);
				$stmt->bindParam(2, $row["Date"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
	}
}

?>