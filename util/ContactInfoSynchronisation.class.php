<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class ContactInfoSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	private $titleMapping = array("m", "w"); 
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("DROP TABLE IF EXISTS contactInfo;");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS contactInfo (id integer PRIMARY KEY AUTOINCREMENT, customer_id integer, running_no integer, position varchar(255), title varchar(10), lastname VARCHAR(255), firstname VARCHAR(255), phone integer, phone_code integer, fax integer, fax_code integer, mobile integer, mobile_code integer, email varchar(255), synched tinyint);");
	}
	
	public function fillTable(){
		$sql_read = "select * from vieAPPKundenKontakte";
		$sql_write = "INSERT INTO contactInfo ( id, customer_id, running_no, position, title, lastname, firstname, phone, phone_code, fax, fax_code, mobile, mobile_code, email, synched ) VALUES ( NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1 );";

		$rows = $this->source_link->query($sql_read);
		foreach ($rows as $row) {
			try {
				foreach ($row as $key => $element)
					if (is_string($element))
						$row[$key] = trim($element);
						
				$stmt = $this->target_link->prepare($sql_write);
				$stmt->bindParam(1, $row["Kundennummer"]);
				$stmt->bindParam(2, $row["Folgenummer"]);
				$stmt->bindParam(3, $row["Funktion"]);
				if (intval($row["Anrede"])>0 && intval($row["Anrede"]) <= sizeof($this->titleMapping))
					$stmt->bindParam(4, $this->titleMapping[intval($row["Anrede"])-1]);
				else
					$stmt->bindParam(4,  $this->titleMapping[0]);
					
				$stmt->bindParam(5, $row["NName"]);
				$stmt->bindParam(6, $row["VName"]);
				$stmt->bindParam(7, $row["TelNr"]);
				$stmt->bindParam(8, $row["TelVorwahl"]);
				$stmt->bindParam(9, $row["FaxNr"]);
				$stmt->bindParam(10, $row["FaxVorwahl"]);
				$stmt->bindParam(11, $row["NatelNr"]);
				$stmt->bindParam(12, $row["NatelVorwahl"]);
				$stmt->bindParam(13, $row["Mail"]);
				$stmt->execute();
			}
			catch (Exception $e){
				echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
			}
		}
	}
}

?>