<?php 
require_once __DIR__.DIRECTORY_SEPARATOR.'SynchronisationTable.interface.php';

class UserSynchronisation implements SyncrhonisationTable{
	private $source_link, $target_link;
	private $ids = array();
	
	function __construct($source, $target){
		$this->source_link = $source;
		$this->target_link = $target;
		$this->dropTable();
		$this->createTable();
		$this->fillTable();
	}
	
	public function dropTable(){
		$this->target_link->query("drop table if exists users");
	}
	
	public function createTable(){
		$this->target_link->query("CREATE TABLE IF NOT EXISTS users ( id integer PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), shortname VARCHAR(10), type VARCHAR(32), password VARCHAR(255), email VARCHAR(255) );");
	}
	
	public function fillTable(){
		$sql_read = "select * from vieAPPVertreter";
		$sql_write = "INSERT INTO users ( id, name, shortname, password, email ) VALUES ( NULL, ?, ?, ?, ? );";
		try {
			$stmt = $this->target_link->prepare($sql_write);
			$rows = $this->source_link->query($sql_read);
			$i = 0;
			foreach ($rows as $row) {
				$i++;
				$stmt->bindParam(1, $row["LoginFullName"]);
				$stmt->bindParam(2, $row["LoginName"]);
				$stmt->bindParam(3, $row["LoginPassword"]);
				$stmt->bindParam(4, $row["EMailAddress"]);
				$stmt->execute();
				$this->ids[$row["LoginName"]] = $i;
			}
		}
		catch (Exception $e){
			echo "Exception while synchronising: ".$e->getMessage()."<br>\n";
		}
	}
	
	public function getIds(){
		return $this->ids;
	}
}

?>