<?php 

interface SyncrhonisationTable{
	
	public function dropTable();
	
	public function createTable();
	
	public function fillTable();
}

?>