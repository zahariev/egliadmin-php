<?php 

require_once 'config.php';
function __autoload($class_name) {
	echo $class_name."<br>\n";
    include "util" . DIRECTORY_SEPARATOR . $class_name . '.class.php';
}

if (!is_writable($egli_file_sqlite))
	die("Cannot write the sqlite file. Am I missing permissions?");
if (!is_writable(dirname($egli_file_tmp)))
	die("Cannot write in the target folder for the zip-File. Am I missing permissions?");
if (file_exists($egli_dir_tmp)){
	if (false !== ($dir = opendir($egli_dir_tmp))){
		while (false !== ($file = readdir($dir))){
			if ($file != '.' && $file != '..'){
				if(!unlink($egli_dir_tmp.DIRECTORY_SEPARATOR.$file))
					die("Cannot write in the target folder for article images. Am I missing permissions?");
			}
		}
	}
}
else if(!mkdir($egli_dir_tmp))
	die("Cannot write in the target folder for the zip-File. Am I missing permissions?");
if (file_exists($egli_file_tmp))
	unlink($egli_file_tmp);

try {
    $target_link = new PDO('sqlite:'.$egli_file_sqlite);
    $target_link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $target_link->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
catch (PDOException $e) {
    die('Cannot connect to the SQLite database: ' . $e->getMessage());
}

try{
	$source_link = new PDO("sqlsrv:server=$source_db_host ; Database=$source_db_database", $source_db_user, $source_db_pass);
    $source_link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $source_link->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
catch(Exception $e){
	die("Cannot connect to the MS SQL Database: ".$e->getMessage());
}

$users = new UserSynchronisation($source_link, $target_link);
/*new ArticleSynchronisation($source_link, $target_link);
new MutationSynchronisation($source_link, $target_link);
new CustomerSynchronisation($source_link, $target_link);
new TranslationSynchronisation($source_link, $target_link);
new ContactInfoSynchronisation($source_link, $target_link);
new CRMSynchronisation($source_link, $target_link);
new CustomerPricesSynchronisation($source_link, $target_link);
new ExpensesSynchronisation($source_link, $target_link);
new HolidaySynchronisation($source_link, $target_link);
new SearchViewSynchronisation($source_link, $target_link);*/
new OrderSynchronisation($source_link, $target_link, $users->getIds());

$old_sqlite_file = $egli_dir_tmp.DIRECTORY_SEPARATOR.$egli_fileonly_sqlite;
if (file_exists($old_sqlite_file))
	unlink($old_sqlite_file);
copy($egli_file_sqlite, $old_sqlite_file);

$zip = new ZipArchive();
$zip->open($egli_file_tmp, ZipArchive::CREATE);
if (false !== ($dir = opendir($egli_dir_tmp))){
	while (false !== ($file = readdir($dir))){
		if ($file != '.' && $file != '..'){
			$zip->addFile($egli_dir_tmp.DIRECTORY_SEPARATOR.$file, $file);
		}
	}
}
else
	die('Can\'t read dir');
$zip->close();

if (file_exists($egli_file_zip))
	unlink($egli_file_zip);

copy($egli_file_tmp, $egli_file_zip);
unlink($egli_file_tmp);

?>